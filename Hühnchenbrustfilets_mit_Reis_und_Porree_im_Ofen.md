# Hühnchen mit Reis und Porre

## Thread 1

-   Hühnchen aus dem Kühlschrank
-   2 halbe Hühnchenbrüste
-   schneiden, vierteln, die dicke seite in der Mitte halbieren
-   2 x 60 cm Alufolie
-   längs und quer legen
-   Öl drauf verteilen
-   1 El Öl in PFanne erhitzen
-   Hühnchen von beiden Seiten mit mittelgroßer Prise Salz
-   Stücke in Pfanne jeweils 2 min von beiden Seiten kräftig anbraten
    Kruste erwünscht
-   fertige Hühchenstücke in Alufolie tun
-   wenn noch reichlich Öl in der Pfanne, OK, sonst 1 EL Öl dazu
    -   2 - 3 Knoblauchzehen zerdrücken und in der Pfanne anbraten, bis es riecht
-   Alufolie zu Schale formen
-   Öl aus der PFanne über die Hühnchen gießen,
-   Alufolie luftdicht verschließen

## Thread 2

-   Reis wie er da in der Auflaufform steht
-   Wasser aufkochen
-   1 TL Salz zum Reis
-   1 TL Öl
-   Wasser zum Reis, bis 1 - 1,5 cm über dem Reis
-   Backofen 250
-   Reis auf mittleres Rost

## Thread 3

-   Porree aus Gefrierschrank
-   in Pfanne 1 El Magarine schmelzen
-   die hälfte vom Porree in Pfanne
-   die andere sofort zurück in den Grfrierschrank
-   wenn farben leuchtend
    -   angebrochene Sahne und ganze Creme Fraiche dazu
    -   mit Salz abschmecken
        -   1/4 Tl, probieren, wiederholen
    -   mit reichlich schwarzem Pfeffer abschmecken
-   wenn anfängt zur köcheln, herunter stellen auf 1, so dass es köchelt, aber nicht zu sehr köchelt
-   nicht zu flüssig
-   falls hphnchen und Reis noch nicht fertig sind
    -   Porree in weiterer Auflaufform im Ofen warmhalten

## Ablauf

-   anfangen um 12:45 Uhr, essen um 13:30 Uhr
-   Thread 1, Thread 2
-   in den Ofen
-   Thread 3
-   jede 10 nach Reis schauen, fertig, wenn kein wasser mehr
-   nach 20 Minten nach hühnchen schauen, fertig, wenn gar
    -   achtung beim öffnen: heißer Dampf!
