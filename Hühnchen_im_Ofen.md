# Hähchenschenkel mit Kartoffeln

## Thread 1: Kartoffeln

-   Kartoffeln schälen
    -   4 - 5 mittelgroße pro Person
    -   halbieren bzw. vierteln
-   Kartoffeln Kochen
    -   Kartoffeln in einen Topf
    -   1,5 TL Salz dazu
    -   Wasser aufkochen
    -   Wasser auf die Kartoffeln
    -   Wasser aufkochen lassen
    -   köcheln lassen
    -   nach 20 Minuten
        -   mit Messer reinpieken
        -   fertig, wenn vom Messer abrutschen
-   Wasser abgießen

## Thread 2: Hähnchenschenkel

-   Hähnchenschenkel zubereiten
    -   Hähnchenschenkel waschen
    -   Hähnchenschenkel abtupfen
    -   evtl. Häärchen entfernen
-   Soße in Tasse zubereiten
    -   4 Knoblauchzehen schälen und auspressen
    -   1 TL Salz dazu
    -   2 - 3 EL Öl
    -   1/2 TL Chilli
-   Zusammenführen
    -   Auflaufform mit Sonnenblumenöl beschmieren
    -   Hähnschenschenkel mit Soße beschmieren
    -   Ohne Deckel in Ofen bei 250°C
    -   nach 20 Minuten rausnehmen und checken:
        -   fertig, wenn Farbe bekommen haben

## Ablauf

-   Kartoffeln, zum Kochen anstellen
-   Hühnchen machen
-   Kartoffeln unter Hühnchen in die Auflaufform
-   Dann wieder zurück in den Ofen
-   15 Minuten
-   Checken, ob Hühnchen noch blutig
-   fertig, wenn nicht, und Farbe da

## Sauce Holandaise

-   Im Mikro warm machen
-   Tisch decken
