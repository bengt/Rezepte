# Brauche

2 - 3 Knoblauchzehen
1 Zwiebel groß
1 Möhre groß
1 Stück 80 g Selerie

# Gemüse Vorbereiten

alles schälen
Reibe in Küchenmaschinen
1 Zwiebel halbieren, vierteln, scheiben schneiden

# Kochen

1 EL Olivenöl in SChnellkochtopf auf 11 erhitzen
1 PAckung 125 / 250 g Schinkenwürfel rein, bis glasig
alles Hackfleisch rein, ständig umrühren bis krümelig (ca 7 Minuten)
1 TL gestrichen Zimt dazu
1/2 TL Salz dazu
Gemüse dazu, deckel drauf (10 Minuten bis weich) alle 3 Minuten umrühren
umrühren
1 Becher Sahne dazu, aufkochen
köcheln auf 1 - 2 bis Sahne eingezogen wird (10 oder 15 Minuten)
300 mL Wein dazu, weiter köcheln lassen, bis Wein verdampft ist
2 EL Tomatenmark dazu, untermengen
2 Dosen gehackte Tomaten dazu, untermengen
Stufe 11, bis aufkocht
Knoblauch reinpressen
2 TL italienische KRäuter (Thymian (bei den Gewürzen), meine HAnd voll Salbei (große flache Blätter)) scheiden, dazugeben
aufkochen
vorsichtig Mit Slaz abschmecken
1 Chilli on Dunstabszugshaube reinbröseln
Andere Herdplatte auf Stufe 1, Topf rüber, DEckel drauf
1 - 1,5 h köcheln lassen, alle 20 Minten umrühren

