## Zutaten

-   Mehl
-   Salz
-   Olivenöl
-   Hefe
-   Wasser

## Zubereitung

-   300 ml Wasser aufkochen.
-   Eine Salatschüssel auf eine Küchenwaage stellen.
-   450 g Mehl einwiegen.
-   50 g Olivenöl einwiegen.
-   1 Teelöffel Salz einstreuen.
-   1 Hefewürfel dazubröseln bzw.  
    1 Packung Trockenhefe einstreuen
-   Alles mit einem Schneebesen durchmischen.
-   300 ml Wasser einwiegen.  
    Es sollte jetzt nicht mehr als 45°C warm sein.
-   Olivenöl bereitstellen.
-   Teig in der Schüssel ca. 5 Minuten kneten.  
    Er sollte dann eine gleichmäßige Oberfläche haben.  
    Falls er zu sehr klebt, hilft etwas Olivenöl.  
-   Die Salatschüssel mit einem sauberen Küchentuch abdecken.
-   Die Salatschüssel warm stellen.  
    Mehr Wärme ist besser, jedoch nicht mehr als 45°C.  
    Die Gärfunktion eines Ofens kann man nutzen.  
    Ein HiFi-Verstärker ist eine gute Wärmequelle, muss aber vorgeheizt werden.
-   40 Minuten lang etwas Anderes machen.
-   Den Ofen auf 250°C vorheizen.
-   Belag nach Wahl vorbereiten.
-   Eine saubere Arbeitsfläche mit Mehl bestreuen.
-   Den Teig ausrollen.  
    Er sollte jetzt etwa auf die doppelte Größe aufgegangen sein.  
    Mit der Hand oder einem Nudelholz.  
    Auf 2 Bleche oder 3 runde Pizzen.
-   Pizzen belegen.  
    Erst Tomatensoße, dann Was-auch-immer, zuletzt Käse.  
    Bonuspunkte, wenn die Aufteilung in Stücke aufgeht.
-   Pizzen nacheinander für jeweils ca. 15 Minuten backen.  
    Der Käse sollte geschmolzen sein.  
    Teig sollte erste Verbrennungen zeigen.  
    First Coal.
-   Pizzen mit etwas grünem Garnieren.
    z.B. Oregano.  
    Bei Belag mit Gemüse als Gegenakzent Kurkuma.  
    Bei süßem Belag wie Hawaii taugt Currypulver.  
    Wer es scharf mag, greift zu Paprikapulver.
-   Gebackene Pizzen schneiden.  
    Auf einem Schneidbrett.  
    Mit einem Pizzaschneider oder einem großen Messer.  
    Junggesellenbonuspunkte für das Zerlegen mit einer Küchenschere.
-   Auf warmen Tellern servieren.  
    Seien wir ehrlich: Besser schmeckt sie eh kalt am nächsten Morgen.