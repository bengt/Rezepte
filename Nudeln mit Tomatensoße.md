Bauchspeck

- halbieren
- 1 Hälfte in Dose, in Kühlschrank
- Haut abnehmen
- quer zur Maserung in dünne Streifen schneiden
- Streifen halbieren/vierteln

Zwiebeln

- 2 nehmen
- schälen
- in Würfel schneiden
- schnellkochtopf auf den hard
- 1 EL Ölivenöl rein
- auf voller Leistung erhitzen
- vorsichtig Bauchspeck rein (kann spritzen)
- ständig umrühren
- bis glasig
- 500 g Hack dazu
- krümelig rühren
- Zwiebeln dazu
- umrühren
- 2 - 3 Knoblauchzehen dazu pressen
- 1 TL Salz dazu
- 1 TL Zimt dazu
- 1 TL Cayennepfeffer dazu
- umrühren
- rotwein dazu
- 1/2 Glas Apfelsaft aus Kühlschrank dazu
- auf 50 %
- ständig umrühren
- köcheln lassen
- ca. 10 Minuten
- 1/2 Becher Sahne rein
- Hitze auf 100 %
- ca. 10 Minuten
- 1 Dose gehackte Tomaten dazu
- umrühren
- wenn dickflüssig, Wasser dazu, aufkochen
- wenn salz fehlt, Salz dazu
- Cayenne
- Knoblauch
- auf 50%
- köcheln lassen

Nudeln kochen
