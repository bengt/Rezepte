## Rausholen

-   300 g Hühnchen
-   3 - 4 Möhren
-   1 Zucchini
-   250 g Pilze
-   2 cm / 20 g / 1 Stück Ingwer
-   2 Knoblauchzehen
-   1 Glas Bambussprossen
-   Raps- oder Sonnenblumenöl
-   Sojasoße

## Vorbereiten

-   Hühnchen in dünne Streifen schneiden
-   Möhren schälen, diagonal in Scheiben schneiden
-   Zucchini abwaschen, Enden abschneiden in 5 mm dicke Scheiben schneiden
-   Pilze trockene Stellen abschneiden, Dreck abreiben, halbieren
-   Ingwer schälen, fein würfeln
-   Knoblauchzehen schälen, in dünne Scheiben schneiden
-   Bambussprossen im Glas von Wasser trennen

## Zubereiten

-   Nudeln
    -   Wasser im Wasserkocher aufkochen
    -   Wasser in Topf geben
    -   Salz in Topf geben
    -   Wasser aufkochen
    -   Nudeln in Topf geben
    -   Wasser aufkochen
    -   Topf von Kochstelle nehmen
    -   Wasser über Nudelsieb abgießen
-   Gemüse
    -   2 EL Öl in Pfanne erhitzen
    -   Hühnchen in Pfanne geben
    -   Hühnchen unter Wenden 2 - 3 Min. lang anbraten
    -   2 -3 EL Soja-Soße in Pfanne geben
    -   Pfanneninhalt umrühren
    -   2 Min. einziehen lassen
    -   Ingwer und Knoblauch in PFanne geben
    -   30 sek. einziehen lassen
    -   Möhren in PFanne geben
    -   Pfanneninhalt durchmischen
    -   2 - 3 Min anbraten
    -   Pilze in Pfanne geben
    -   Pfanneninhalt durchmischen
    -   Zucchini in Pfanne geben
    -   Pfanneninhalt durchmischen
    -   Bambussprossen in Pfanne geben
    -   Pfanneninhalt durchmischen
    -   2 EL Soja-Soße in Pfanne geben
    -   Pfanneninhalt durchmischen
    -   Deckel auf Pfanne
    -   2 -3 Min einziehen lassen
    -   Pfanneninhalt durchmischen

## Abschmecken

-   Solange Nudeln noch nicht braun, Sojasoße dazu, umrühren 
-   Pfanne von Herd, servieren
