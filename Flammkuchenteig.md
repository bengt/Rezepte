300g Mehl
170 ml lauwarmes Wasser
1/4 TL Trockenes Hefe (liegt bei Backpulver) od. 3 gr Würfelhefe
1TL Salz
2 EL Öl

Hefe im lauwarmen Wasser zerlassen. Mehl und Salz mischen. Hefe-Wasser gemisch  dazu. Etwas kneten. Dann Öl dazu. Wieder etwas kneten (ganz kurz) 20 Minuten ruhen lassen und danach ganz gut kneten bis der Teig ganz glatt ist. Eine Schällchen (grüne Tupper-Schussel) mit Öl beschmieren, Teig zu einer Kugel formen und in die Schussel reintun, Mit frischhaltefolie abdecken. In Ruhe lassen.
